# FunnyBot
A proof-of-concept slackbot (C) 2018 Ronni Jensen

FunnyBot requires python => 3.6.5

Installation:

clone the repository with 
```
git clone https://MarcoDePollo@bitbucket.org/MarcoDePollo/funnybot.git funnybot
```
Setup the virtual environment
```
python3 -m venv funnybot
```

Activate the virtual enviroment

```
source funnybot/bin/activate
```

And install slackclient

```
pip3 install slackclient
```

Start the bot. The bot reads an enviroment-variable called SLACK_BOT_TOKEN to get the access-token to Slack, so start it with:
```
SLACK_BOT_TOKEN="your access-token here" python3 funnybot.py
```

